<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die;

global $CFG, $DB;

require_once($CFG->dirroot.'/blocks/metadata_status/locallib.php');

if ($ADMIN->fulltree) {
    $settings->add(new admin_setting_heading('block_metadata_status/metadata_tracking_configuration',
            get_string('config_header_block_configuration', 'block_metadata_status'),
            get_string('config_header_block_configuration_desc', 'block_metadata_status'))
    );

    $fields = $DB->get_records_menu('local_metadata_field', [ 'contextlevel' => CONTEXT_MODULE ], '', 'id, name');
    $fieldsmenu = [ 0 => get_string('none') ] + array_map('format_string', $fields);
    $settings->add(new admin_setting_configselect('block_metadata_status/metadatasharedfield',
            get_string('config_shared_metadata', 'block_metadata_status'),
            get_string('config_shared_metadata_desc', 'block_metadata_status'),
            0,
            $fieldsmenu
            ));
    $settings->add(new admin_setting_configselect('block_metadata_status/metadatalicensefield',
            get_string('config_license_metadata', 'block_metadata_status'),
            get_string('config_license_metadata_desc', 'block_metadata_status'),
            0,
            $fieldsmenu
            ));

    $settings->add(new admin_setting_configselect('block_metadata_status/progress_bar_threshold',
            get_string('config_progress_bar_threshold', 'block_metadata_status'),
            get_string('config_progress_bar_threshold_desc', 'block_metadata_status'),
            7,
            range(0, 90, 10)
    ));

    $settings->add(new admin_setting_configcheckbox(
            'block_metadata_status/enable_percentage_label',
            get_string('config_enable_percentage', 'block_metadata_status'),
            get_string('config_enable_percentage_desc', 'block_metadata_status'),
            1
    ));

    /* ---------------------------------------------------------------------------------------------------------- */

    $settings->add(new admin_setting_heading('block_metadata_status/metadata_tracking',
            get_string('config_header_metadata_tracking', 'block_metadata_status'),
            get_string('config_header_metadata_tracking_desc', 'block_metadata_status')
    ));

    $moduleMetadataFields = block_metadata_status_get_module_metadata_fields();

    foreach ($moduleMetadataFields as $metadataField) {
        $locked = $metadataField->locked === '1';
        $settings->add(new admin_setting_configcheckbox(
            'block_metadata_status/enable_metadata_' . $metadataField->id . '_tracking',
            ($locked ? '<div class="icon fa fa-lock"></div>' : '').'<b>' . $metadataField->name . '</b> <code> - ' . get_string('displayname', 'metadatafieldtype_' . $metadataField->datatype) . '</code>',
            '',
            $locked ? 0 : 1
        ));
    }

    /* ---------------------------------------------------------------------------------------------------------- */

    $settings->add(new admin_setting_heading('block_metadata_status/metadata_status_customization',
        get_string('config_header_metadata_customization', 'block_metadata_status'),
        get_string('config_header_metadata_customization_desc', 'block_metadata_status')
    ));

    $settings->add(new admin_setting_configcolourpicker(
            'block_metadata_status/progress_bar_background_color',
            get_string('config_progress_bar_background_color', 'block_metadata_status'),
            get_string('config_progress_bar_background_color_desc', 'block_metadata_status'),
            '#D3D3D3'
    ));

    $settings->add(new admin_setting_configcolourpicker(
            'block_metadata_status/progress_bar_color_before_threshold',
            get_string('config_progress_bar_color_before_threshold', 'block_metadata_status'),
            get_string('config_progress_bar_color_before_threshold_desc', 'block_metadata_status'),
            '#FF0000'
    ));

    $settings->add(new admin_setting_configcolourpicker(
            'block_metadata_status/progress_bar_color_after_threshold',
            get_string('config_progress_bar_color_after_threshold', 'block_metadata_status'),
            get_string('config_progress_bar_color_after_threshold_desc', 'block_metadata_status'),
            '#008000'
    ));
}
