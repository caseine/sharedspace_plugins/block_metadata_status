<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

function block_metadata_status_get_module_metadata_fields() {
    global $DB;
    $sql = "SELECT f.id, f.name, f.shortname, f.datatype, f.locked
              FROM {local_metadata_field} f
              JOIN {local_metadata_category} c ON c.id = f.categoryid
             WHERE f.contextlevel = :module AND f.datatype != 'checkbox'
          ORDER BY c.sortorder, f.sortorder";
    $params = [ 'module' => CONTEXT_MODULE ];
    return $DB->get_records_sql($sql, $params);
}

function block_metadata_status_get_tracked_module_metadata_fields() {
    $adminconfig = get_config('block_metadata_status');
    return array_filter(block_metadata_status_get_module_metadata_fields(), function($field) use ($adminconfig) {
        return !empty($adminconfig->{'enable_metadata_' . $field->id . '_tracking'}) && !$field->locked;
    });
}
