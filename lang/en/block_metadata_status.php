<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

$string['pluginname'] = 'Metadata Status';
$string['metadata_status:addinstance'] = 'Add a new Metadata Status block';
$string['metadata_status:addinstance'] = 'Add a new Metadata Status block to the dashboard';
$string['shared_modules'] = 'Shared modules';
$string['filled_modules'] = 'Filled modules';
$string['filled_modules_help'] = 'Modules are considered filled when all the following metadata are filled in:
{$a}';
$string['shared'] = 'Shared: {$a}';
$string['license'] = 'License: {$a}';

$string['config_header_block_configuration'] = 'Metadata Status block configuration';
$string['config_header_block_configuration_desc'] = '';
$string['config_shared_metadata'] = 'Metadata "Shared" field';
$string['config_shared_metadata_desc'] = 'Used to decide which activity should be displayed as shared';
$string['config_license_metadata'] = 'Metadata "License" field';
$string['config_license_metadata_desc'] = 'Used to display License information on hover of activities';
$string['config_enable_percentage'] = 'Enable percentage label';
$string['config_enable_percentage_desc'] = '';
$string['config_header_metadata_tracking'] = 'Metadata tracking';
$string['config_header_metadata_tracking_desc'] = '<span style="color: #FF0000;">Checkboxes cannot be tracked because it is impossible to determine if they are filled or not.</span>';
$string['config_header_metadata_customization'] = 'Metadata status customization';
$string['config_header_metadata_customization_desc'] = '';
$string['config_progress_bar_background_color'] = 'Progress bar background color';
$string['config_progress_bar_background_color_desc'] = '';
$string['config_progress_bar_threshold'] = 'Progress bar threshold';
$string['config_progress_bar_threshold_desc'] = '';
$string['config_progress_bar_color_before_threshold'] = 'Progress bar color before threshold';
$string['config_progress_bar_color_before_threshold_desc'] = '';
$string['config_progress_bar_color_after_threshold'] = 'Progress bar color after threshold';
$string['config_progress_bar_color_after_threshold_desc'] = '';
