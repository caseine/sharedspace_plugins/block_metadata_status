<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

$string['pluginname'] = 'Suivi des Métadonnées';
$string['metadata_status:addinstance'] = 'Ajouter un nouveau bloc Suivi des Métadonnées';
$string['metadata_status:myaddinstance'] = 'Ajouter un nouveau bloc Suivi des Métadonnées à ma page Moodle';
$string['shared_modules'] = 'Modules partagés';
$string['filled_modules'] = 'Modules remplis';
$string['filled_modules_help'] = 'Un module est considéré comme rempli quand les métadonnées suivantes sont remplies :
{$a}';
$string['shared'] = 'Partagé : {$a}';
$string['license'] = 'License : {$a}';

$string['config_header_block_configuration'] = 'Configuration du bloc Suivi des Métadonnées';
$string['config_header_block_configuration_desc'] = '';
$string['config_shared_metadata'] = 'Métadonnée de partage';
$string['config_shared_metadata_desc'] = 'Utilisée pour décider si une activité doit être affichée comme partagée';
$string['config_license_metadata'] = 'Métadonnée de license';
$string['config_license_metadata_desc'] = 'Utilisée pour afficher la license au survol d\'une activité';
$string['config_enable_percentage'] = 'Activer l\'étiquette de pourcentage';
$string['config_enable_percentage_desc'] = '';
$string['config_header_metadata_tracking'] = 'Suivi des métadonnées';
$string['config_header_metadata_tracking_desc'] = '<span style="color: #FF0000;">Les cases à cocher ne peuvent pas être suivies car il est impossible de savoir si elles sont remplies ou non.</span>';
$string['config_header_metadata_customization'] = 'Personnalisation de Suivi des Métadonnées';
$string['config_header_metadata_customization_desc'] = '';
$string['config_progress_bar_background_color'] = 'Couleur de fond de la barre de progression';
$string['config_progress_bar_background_color_desc'] = '';
$string['config_progress_bar_threshold'] = 'Seuil de la barre de progression';
$string['config_progress_bar_threshold_desc'] = '';
$string['config_progress_bar_color_before_threshold'] = 'Couleur de la barre de progression avant le seuil';
$string['config_progress_bar_color_before_threshold_desc'] = '';
$string['config_progress_bar_color_after_threshold'] = 'Couleur de la barre de progression après le seuil';
$string['config_progress_bar_color_after_threshold_desc'] = '';
