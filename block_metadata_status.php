<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

class block_metadata_status extends block_base implements renderable, templatable {

    protected $modulesdata = null;

    function has_config() {
        return true;
    }

    /**
     * Enable to add the block only in a course
     *
     * @return array
     */
    public function applicable_formats() {
        return [
            'site-index' => true,
            'course-view' => true,
        ];
    }

    /**
     * Allow only one instance of the block on a page
     *
     * @return bool
     */
    function instance_allow_multiple() {
        return false;
    }

    protected function should_display_block() {
        global $COURSE, $CFG;

        // Display Metadata Status when on forager role, and when editing.
        require_once($CFG->dirroot . '/blocks/sharing_cart/locallib.php');
        $context = context_course::instance($COURSE->id);
        $visitor = block_sharing_cart_is_visitor($context);

        return $this->page->user_is_editing() || $visitor;
    }

    protected function get_modules_data() {
        if ($this->modulesdata === null) {
            global $CFG, $DB, $COURSE;

            require_once($CFG->dirroot.'/blocks/metadata_status/locallib.php');

            $sharedfieldid = get_config('block_metadata_status', 'metadatasharedfield') ?: 0;
            $licensefieldid = get_config('block_metadata_status', 'metadatalicensefield') ?: 0;
            $trackedfieldsids = array_column(block_metadata_status_get_tracked_module_metadata_fields(), 'id');

            $this->modulesdata = [];

            list($insql, $inparams) = $DB->get_in_or_equal(array_merge($trackedfieldsids, [ $sharedfieldid, $licensefieldid ]), SQL_PARAMS_NAMED);

            foreach (get_fast_modinfo($COURSE->id, -1)->cms as $cm) {
                if ($cm->modname == 'label') {
                    // Ignore labels.
                    continue;
                }
                $sql = "SELECT f.id, md.data, f.defaultdata
                          FROM {local_metadata_field} f
                          JOIN {local_metadata} md ON md.fieldid = f.id
                         WHERE md.instanceid = :cmid AND f.id $insql";
                $metadata = $DB->get_records_sql($sql, $inparams + [ 'cmid' => $cm->id ]);

                $module = new stdClass();
                $module->id = $cm->id;
                $module->shared = !empty($metadata[$sharedfieldid]->data);
                if (!empty($metadata[$licensefieldid]->data)
                        && $metadata[$licensefieldid]->data != $metadata[$licensefieldid]->defaultdata) {
                    $module->license = get_string('license', 'block_metadata_status', $metadata[$licensefieldid]->data);
                } else {
                    $module->license = '';
                }

                if (!empty($trackedfieldsids)) {
                    $nfilled = 0;
                    foreach ($trackedfieldsids as $fieldid) {
                        if (!empty($metadata[$fieldid]->data) && $metadata[$fieldid]->data != $metadata[$fieldid]->defaultdata) {
                            $nfilled ++;
                        }
                    }
                    $module->percentage = intval(round((100 * $nfilled) / count($trackedfieldsids)));
                }
                $this->modulesdata[] = $module;
            }
        }
        return $this->modulesdata;
    }

    /**
     * Block initializations
     *
     * @throws coding_exception
     */
    public function init() {
        $this->title = get_string('pluginname', 'block_metadata_status');
    }

    /**
     * Block contents
     *
     * @return string
     *
     * @throws coding_exception
     */
    public function get_content() {
        if ($this->content !== null) {
            return $this->content;
        }

        $this->content         =  new stdClass;
        $this->content->text   = '';
        $this->content->footer = '';

        if ($this->should_display_block()) {
            $this->content->text = $this->page->get_renderer('block_metadata_status')->render($this);
        }

        return $this->content;
    }

    public function get_required_javascript() {
        if ($this->should_display_block()) {
            // Create and place a node containing data for the javascript.
            $adminconfig = get_config('block_metadata_status');
            $datanode = html_writer::span('', 'block_metadata_status d-none', [
                    'data-blockdata' => json_encode($this->get_modules_data()),
            ]);
            $cssnode = '<style type="text/css">
                            .block_metadata_status_wrapper {
                                --block_metadata_status-bar-bg: '. $adminconfig->progress_bar_background_color . ';
                                --block_metadata_status-color-bt: '. $adminconfig->progress_bar_color_before_threshold . ';
                                --block_metadata_status-color-at: '. $adminconfig->progress_bar_color_after_threshold . ';
                            }
                        </style>';
            $this->page->requires->js_init_code('document.getElementsByClassName("course-content")[0]
                                                .insertAdjacentHTML("beforeend", "' . addslashes_js($datanode . $cssnode) . '");');
            $this->page->requires->string_for_js('shared', 'block_metadata_status');
            $this->page->requires->strings_for_js([ 'yes', 'no' ], 'moodle');
            $this->page->requires->js_call_amd('block_metadata_status/script_metadata_status', 'init',
                    [ $adminconfig->progress_bar_threshold * 10, $adminconfig->enable_percentage_label ]);
        }
    }

    public function export_for_template(renderer_base $output) {
        global $CFG;
        require_once($CFG->dirroot.'/blocks/metadata_status/locallib.php');
        return [
                'sharedModules' => count(array_filter($this->get_modules_data(), function($module) {
                            return $module->shared;
                        })),
                'filledModules' => count(array_filter($this->get_modules_data(), function($module) {
                            return isset($module->percentage) && $module->percentage === 100;
                        })),
                'filledModulesHelp' => get_string('filled_modules_help', 'block_metadata_status',
                        '&bull; ' . implode("\n&bull; ", array_map('format_string', array_column(block_metadata_status_get_tracked_module_metadata_fields(), 'name')))),
        ];
    }

}
