<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace block_metadata_status\output;

use block_metadata_status;
use plugin_renderer_base;

class renderer extends plugin_renderer_base {

    /**
     * Return the main content for the block metadata_status.
     *
     * @param block_metadata_status $metadata_status The block_metadata_status renderable
     *
     * @return string HTML string
     */
    public function render_block_metadata_status(block_metadata_status $metadata_status) {
        return $this->render_from_template('block_metadata_status/metadata_status', $metadata_status->export_for_template($this));
    }
}
